require_relative 'console'
require_relative 'devices'

require "mini_magick"

class PrettyFrames
  def initialize(device_frame_image_directory, frame_shadow)
    @device_frame_image_directory = device_frame_image_directory
    @frame_shadow = frame_shadow
  end

  def frame(device_name, device, colour)
    device_frame = device[:device_frame]
    sub_directory = @frame_shadow ? device_frame[:sub_directory_with_shadow] : device_frame[:sub_directory]

    # fallback to using the first colour for this device, if all else fails
    filename = device_frame[:colours][colour.to_sym] unless colour.nil?
    filename = device_frame[:colours].values[0] if colour.nil? || filename.nil?

    if filename.nil?
      log_warn("Available colours for device #{device_name}:")
      device_frame[:colours].each { |k, v|
        log_warn(" #{k.to_s}")
      }
      app_fail("Colour #{colour} not found for this device")
    end

    image_path = "#{@device_frame_image_directory}/#{sub_directory}/#{filename}"
    app_fail("No pretty frame found at path #{image_path}") unless File.file?(image_path)

    MiniMagick::Image.open(image_path)
  end

  def best_frame(source_images, partial_name, all_devices, ideal_width, ideal_height)
    # this is a hash of all the devices which have pretty frames,
    # key: framename symbol, value: hash with :width :height values for screen size
    available_frame_sizes = available_frame_sizes(all_devices)

    # this is the sizes available for the given partial filename,
    # a hash: key filepath, value geometry
    available_screenshot_sizes = source_images.available_screenshot_sizes_for_partial_name(partial_name)

    # now filter the frame sizes for only those which have an available image
    possible_frames = available_frame_sizes.select do |frame_name, size|
      available_screenshot_sizes.values.include? size
    end

    app_fail("Could not find a correctly-sized pretty frame for input file '#{partial_name}'") if possible_frames.count == 0

    # now find the best one from this collection
    # do this by finding the frame whose screen dimensions are the closest to the
    # ideal_width and ideal_height

    best_score = -100
    best_frame_name = nil
    ideal_aspect_ratio = ideal_width.to_f / ideal_height
    ideal_area = ideal_width * ideal_height

    possible_frames.each do |frame_name, screen_size|
      width = screen_size[:width]
      height = screen_size[:height]
      aspect_ratio = width.to_f / height
      area = width * height
      aspect_ratio_closeness = [aspect_ratio, ideal_aspect_ratio].min.to_f / [aspect_ratio, ideal_aspect_ratio].max
      area_closeness = [area, ideal_area].min.to_f / [area, ideal_area].max
      score =  (aspect_ratio_closeness * 10) + (area_closeness * 5)
      if best_frame_name.nil? || score > best_score
        best_score = score
        best_frame_name = frame_name
      end
    end

    best_frame_name
  end

  def available_frame_sizes(all_devices)
    device_size_map = all_devices.reduce({}) do |device_size_map, device_name_and_device|
      device_name = device_name_and_device[0]
      device = device_name_and_device[1]

      frame = device[:device_frame]
      device_size_map[device_name] = {:width => device[:width], :height => device[:height]} unless frame.nil?
      device_size_map
    end

    device_size_map
  end
end
