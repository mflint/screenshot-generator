#!/usr/bin/env ruby

require_relative 'console'
require_relative 'devices'
require_relative 'canvas'
require_relative 'sources'
require_relative 'pretty_frames'

require 'optparse'
require 'json'

def parse_options(args)
  options = {}
  options[:config_file] = nil
  options[:input_image_directory] = nil
  options[:device_frame_image_directory] = nil
  options[:frame_shadow] = false
  options[:output_image_directory] = '.'
  options[:dry_run] = false
  options[:whole_canvas] = false
  options[:verbose] = []

  option_parser = OptionParser.new {|opts|
    opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

    opts.on('-c', '--config CONFIG_FILE', 'Use the given config file') do |config_file|
      options[:config_file] = config_file
    end

    opts.on('-i', '--input INPUT_IMAGE_DIRECTORY', 'Use input images from this directory') do |input_image_directory|
      options[:input_image_directory] = input_image_directory
    end

    opts.on('-f', '--frames FRAMES_IMAGE_DIRECTORY', 'Use device frame images from this directory. Optional.') do |device_frame_image_directory|
      options[:device_frame_image_directory] = device_frame_image_directory
    end

    opts.on('-s', '--shadow', 'Use device image frames with shadows') do |frame_shadow|
      options[:frame_shadow] = true
    end

    opts.on('-o', '--output OUTPUT_IMAGE_DIRECTORY', 'Write output to this directory Default: current directory') do |output_image_directory|
      options[:output_image_directory] = output_image_directory
    end

    opts.on('-d', '--dry-run', 'Do a quick dry-run') do |dry_run|
      options[:dry_run] = true
    end

    opts.on('-w', '--whole-canvas', 'Write the whole canvas, not individual files') do |dry_run|
      options[:whole_canvas] = true
    end

    opts.on('-v', '--verbose', 'Run verbosely; repeat for more verbosity') do |verbose|
      options[:verbose] << verbose
    end
  }

  option_parser.parse!(args)

  if options[:config_file].nil?
    puts "Must specify a config file\n"
    puts option_parser
    exit 1
  end

  Console.new(options[:verbose])

  options
end

def make_device_screenshots(device_set, output_device_name, output_device, source_images, all_devices, pretty_frames, language, frame_colour)
  log_info("Making screenshots for #{output_device_name}")

  screen_count = device_set[:screens]
  background = device_set[:background]
  canvas = Canvas.new(@canvas_scale, output_device_name, output_device, screen_count, background)

  # add framed images
  if device_set[:frames]
    device_set[:frames].each do |frame|
      canvas.add_frame(frame, source_images, all_devices, pretty_frames, frame_colour)
    end
  end

  # add images
  if device_set[:images]
    device_set[:images].each do |image|
      canvas.add_image(image)
    end
  end

  # add text
  text_styles = device_set[:text_styles]
  texts = device_set.fetch(:texts, [])
  texts.each do |text|
    style_name = text[:style]

    if style_name.nil?
      style = { :size => 1, :colour => '#ffffff'}
    else
      style = text_styles[style_name.to_sym]
      app_fail("Unknown style '#{style_name}'") if style.nil?
    end

    canvas.add_text(text, language, style)
  end

  # save
  canvas.save(language, @options[:whole_canvas], @options[:output_image_directory])
end

def write_html(image_sets)
  html = "<html>\n<head>\n<style>\n"
  html << ".flex-container { display: flex; flex-wrap: nowrap; }\n"
  html << "</style>\n</head>\n<body>\n"

  for image_set in image_sets do
    scaled_canvas_width = image_set[:canvas_width] * @html_scale
    scaled_image_width = image_set[:image_width] * @html_scale
    scaled_image_height = image_set[:image_height] * @html_scale
    scaled_gap = image_set[:gap] * @html_scale

    html << "<h1>#{image_set[:device]} - #{image_set[:language]}</h1>\n"
    html << "<div class=\"flex-container\" style=\"width: #{scaled_canvas_width}px\">\n"

    for output_file in image_set[:output_files]
      html << "<a href=\"#{output_file}\">"
      html << "<img src=\"#{output_file}\" style=\"margin-right: #{scaled_gap}px; width: #{scaled_image_width}px; height:#{scaled_image_height}px;\"/>\n"
      html << '</a>'
    end

    html << "</div>\n"
  end

  html << "</body>\n"

  html_file = "#{@options[:output_image_directory]}/output.html"
  File.open(html_file, 'w') { |file| file.write(html) }
  system %{open "#{html_file}"}
end

def make_screenshots(args)
  # parse the options
  @options = parse_options(args)
  log_fine("Options #{@options}")

  # load the config file
  config_filename = @options[:config_file]
  app_fail("Config file '#{config_filename}' not found") unless File.file? config_filename
  config_file = open(config_filename)
  config_json = config_file.read
  config = JSON.parse(config_json, symbolize_names: true)

  # first checks
  languages = config[:languages]
  app_fail("Missing 'languages' value") if languages.nil?
  app_fail("Invalid language count") if languages.count < 1

  # for dry-runs, we can make a quarter-sized image
  # for real runs, we make full-sized images, but scale the html output
  @canvas_scale = @options[:dry_run] ? 0.25 : 1
  @html_scale = @options[:dry_run] ? 1 : 0.25

  image_sets = []

  pretty_frames = nil
  pretty_frames = PrettyFrames.new(@options[:device_frame_image_directory], @options[:frame_shadow]) unless @options[:device_frame_image_directory].nil?
  all_devices = Devices.new.all_devices

  # loop through the languages
  languages.each do |language|
    # make an index of the source images
    source_images = Sources.new(@options[:input_image_directory], language)

    # loop through the device_sets
    device_sets = config[:device_sets]
    device_sets.each do |device_set|
      # check the config for this device_set
      screen_count = device_set[:screens]
      app_fail("Missing 'screens' value") if screen_count.nil?
      app_fail("Invalid screen count") if screen_count < 0
      app_fail("Missing 'background'") if device_set[:background].nil?
      has_frames = !device_set[:frames].nil? && device_set[:frames].count > 0
      app_fail('Must specify an input image directory if frames are used') if has_frames && @options[:input_image_directory].nil?

      # find the required device definitions for each device we're making
      # output for
      output_devices = device_set[:devices].reduce({}) do |output_device_map, required_device_name|
        output_device = all_devices[required_device_name.to_sym]
        app_fail("Unknown device: '#{required_device_name}'") if output_device.nil?
        output_device_map[required_device_name] = output_device
        output_device_map
      end

      frame_colours = device_set[:frame_colours]

      # loop through the devices
      output_devices.each do |output_device_name, output_device|
        frame_colour = frame_colours[output_device_name.to_sym] unless frame_colours.nil?
        image_sets << make_device_screenshots(device_set, output_device_name, output_device, source_images, all_devices, pretty_frames, language, frame_colour)
      end if screen_count > 0
    end

  end

  write_html(image_sets)
end


make_screenshots(ARGV)
