require_relative 'console'

require "mini_magick"

class Sources
  def initialize(input_image_directory, language)
    @input_files = {}

    if input_image_directory.nil? || language.nil?
      return
    end

    log_info('Scanning input files')

    entries = Dir.glob("#{input_image_directory}/#{language}/*").select {|f| File.file? f}
    entries.each do |filepath|
      input_image = MiniMagick::Image.open(filepath)
      geometry = input_image.data['geometry']
      @input_files[filepath] = { :width => geometry['width'], :height => geometry['height'] }
      log_fine("Scanning #{language} #{File.basename(filepath)}: #{geometry['width']}x#{geometry['height']}")
    end
  end

  def image(partial_name, required_width, required_height)
    # filter all the input files for the given partial filename
    matching_path_names = @input_files.select {|file_path, geometry|
      filename = File.basename(file_path)
      filename.include? partial_name
    }

    # now find the best image for the given width/height
    best_image_path = nil
    matching_path_names.each do |file_path, geometry|
      width = geometry[:width]
      height = geometry[:height]

      if width == required_width &&
          height == required_height &&
          best_image_path.nil?
        best_image_path = file_path
      end
    end

    app_fail("No input files found for '#{partial_name} #{required_width}x#{required_height}'") if best_image_path.nil?
    log_fine("Found best image: #{best_image_path}")

    MiniMagick::Image.open(best_image_path)
  end

  # @return hash, key filepath, value hash with :width and :height values
  def available_screenshot_sizes_for_partial_name(partial_name)
    # filter all the input files for the given partial filename
    result = {}
    @input_files.each {|file_path, geometry|
      filename = File.basename(file_path)
      result[file_path] = geometry if filename.include? partial_name
    }

    result
  end
end