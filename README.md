# screenshot-generator

A Ruby app for making AppStore screenshots.

# How is this different from fastlane's `frameit` tool?

[frameit] works by taking an _input file_ (usually a screenshot from your app) and creating from that an _output_
file (an image which can be uploaded to the AppStore). This means there's a one-to-one mapping between input and
output, and there's always exactly one screenshot in every AppStore image.

This imaginatively-named `screenshot-generator` tool doesn't have that rule.

There are some other advantages too:
* relative positioning of text, rather than absolute x,y coordinates, which works fairly well across all screen sizes
* support for different _sets_ of devices, so you can have a set of similar images for iPhone, and another set of similar
images for iPad

`screenshot-generator` works by drawing text and screenshots into a single wide canvas, then splitting that canvas
into separate images for the AppStore. 

# How is it similar to `frameit`?

`screenshot-generator` can take screenshots made by [snapshot] as its inputs, and creates output that can be
uploaded to the AppStore with [deliver]. 

# Example output

This set of images for [my tl;dr app][tldr-app] was [created with this json file][example-tldr-json].

![Example image][example1-png]

# Usage

Here's the complete set of options:

```
Usage: generator.rb [options]
    -c, --config CONFIG_FILE         Use the given config file
    -i INPUT_IMAGE_DIRECTORY,        Use input images from this directory
        --input
    -f FRAMES_IMAGE_DIRECTORY,       Use device frame images from this directory. Optional.
        --frames
    -s, --shadow                     Use device image frames with shadows
    -o OUTPUT_IMAGE_DIRECTORY,       Write output to this directory Default: current directory
        --output
    -d, --dry-run                    Do a quick dry-run
    -w, --whole-canvas               Output the whole canvas in a single wide image, not individual files
    -v, --verbose                    Run verbosely; repeat for more verbosity
```

The options are as follows:

| option     | description  | example |
| ---------- | ------------ | ------- |
| -c | The json config file, which describes the layout of the images | -c /path/to/config.json |
| -i | Directory of screenshot input images. This must contain subdirectories for each language code (eg. `en-GB` or `pt-BR) | -i /path/to/screenshots/  |
| -f | Directory for the pretty Facebook frames. Optional | -f /path/to/Facebook\ Devices/ |
| -s | Use Facebook frames with shadows. Optional. Default false | -s |
| -o | Output directory | -o /path/to/output-directory/ |
| -d | Dry run; this runs faster and makes outputs smaller images | -d |
| -w | Output the whole canvas as one image, instead of individual images | -w |
| -v | Verbose logging. Use twice for extra verbose | -v |

# Getting started

You'll need to install ImageMagick first, and run `bundle update` to install the MiniMagick gem.

Start by copying [this minimal example][example-minimal-json] into your project.

This guide assumes you have some screenshots of your app, in the directory `~/MyProject/screenshots/en-GB`. (If your
language code is different, change it in the `languages` section of  `minimal.json`).

You'll also need to edit the names of the screenshots inside `minimal.json`. The example file uses `CommandList.png`
and `CommandDetail.png`, but yours will be different. The names don't need to match exactly - a partial match will do.

Now run the generator:

```
./generator.rb -c ~/MyProject/minimal.json -i ~/MyProject/screenshots/ -o ~/MyProject/screenshots/
```

You should end up with something like this:

![minimal example][example2-png]

## What's in `minimal.json`?

[`minimal.json`][example-minimal-json] defines:
* one language code (`en-GB`)
* one set of devices, which outputs images for one device (`iphone-se`), suitable for upload to the AppStore
* `"screens": 2`: the image will be painted across a canvas, then saved as two iPhone SE sized images
* a background gradient between two blues
* two framed screenshots. The `x_location` value is a floating-point value where `1` and `2` is in the middle of screens
1 and 2. `1.5` would draw the screenshot in between the two images
* two pieces of text for the `en-GB` locale. Here, `screen` value is intended to be an integer value

 # More details
 
See [the JSON file for my tl;dr app][example-tldr-json] for a more complete example, which makes the first image in
this README.
 
## Languages
 
Specify multiple language codes in the `languages` section. These codes must match the folder names inside your
"imput image" directory.
 
Text items in the JSON file must contain a localised string for each of your languages:

```
"languages": [
    "en-GB",
    "en-US"
]
```
```
"texts": [
    {
        "en-GB": "Trousers!",
        "en-US": "Pants!",
        "screen": 1
    }
]
``` 

## Devices

You'll want to create AppStore images for multiple device sizes:

```
"devices": [
    "iphone_se",
    "iphone_8",
    "iphone_8plus",
    "iphone_xs",
    "iphone_xs_max"
]
```

See [devices.rb] for a complete list of device names.

_Tip: when experimenting with layouts, limit yourself to just the smallest and largest devices: `iphone_se` and
`iphone_xs_max`._

## Canvas background

For the background, you must specify two colours of a gradient. A `gradient_angle` can also be given:

```
"background": {
    "gradient_from": "#266594",
    "gradient_to": "#3f92d1",
    "gradient_angle": 20
}
```

## Framed screenshots

You can define a framed screenshot like this:

```
"frames": [
    {
        "source": "CommandList.png",
        "x_location": 0.95,
        "y_location": 0,
        "scale": 1.1,
        "rotation": 8
    }
]
```

The `source` is a partial filename; the script will find all files whose name contains the `source` value, and use
the file with the best size/aspect ratio for the output. So an image for iPhone SE will try to use iPhone SE
screenshots, if available, and iPhone Xs images will use iPhone Xs screenshots.

If the perfect match can't be found - maybe you're generating images for iPhone 8, and there are no screenshots
for iPhone 8 - then it will find the closest match and output a warning.

The `y_location` value of `0` is close to the top of the image, and a value of `1` would be just below the bottom. You can
use slightly negative numbers (`-0.1`) to move the screenshot up.

Examples:

![examples of different y_location values][example-screenshot-y-location-png]

Similarly, `x_location`, `scale` and `rotation` can be used to change how the screenshot is drawn:
 
![examples of different x_location values][example-screenshot-x-location-png]

![examples of different scale values][example-screenshot-scale-png]

![examples of different rotation values][example-screenshot-rotation-png]

## Text

Text _does not_ auto-wrap around other things, so for multi-line text you'll need to add line-breaks (`\n`) into
your json file. It can be aligned `left`, `centre`/`center` or `right`:

![examples of text alignment][example-text-alignment-png]

The `y_location` of the text can be changed; `0` (the default) is at the top of the image, `1` is at the bottom:

![examples of text y_location][example-text-y-location-png]


Styles can be specified like this:

```
"text_styles": {
  "style1": {
    "font": "Tahoma",
    "size": 1,
    "colour": "#ffffff"
  },
  "style2": {
    "font": "Impact",
    "size": 2.5,
    "colour": "#ffff80"
  },
  "style3": {
    "font": "ComicSans",
    "size": 1.5,
    "colour": "#ff8080"
  }
},
"texts": [
  {
    "en-GB": "small\nwhite\ntahoma"
    "style": "style1",
    "screen": 1
  },
  {
    "en-GB": "Big\nImpact",
    "style": "style2",
    "screen": 2
  },
  {
    "en-GB": "Haha,\nComicSans",
    "style": "style3",
    "screen": 3
  }
]
```

... which gives this output:

![examples of text styles][example-text-styles-png]

_Tip: use `convert -list font` to list the fonts available to ImageMagick._

## Adding images

You can add an image like this:

```
"images": [
  {
    "source": "examples/star.png",
    "screen": 0,
    "scale": 0.5,
    "x_location": 0,
    "y_location": 0
  }
]
```

The default size is "aspect fit" - so the image would be scaled to fill the output image - and you can adjust the
scale with the `scale` property. Here, the value `0.5` means the star image will be half the width of the output
image.

`screen` specifies which screen the image appears on; `x_location` is the x position on that screen, where `0` is
the far left and `1` is the far right. Similarly, a `y_location` of `0` is at the top-aligned, `1` is at bottom-aligned.

Example:

![examples of image positioning][example-image-import]

## Multiple `device_sets`

`device_sets` allow you to group sets of images for similar devices.

For example, you might have a device set for iPhone images, each comprising 3 screens, and another device set for
iPad images, each comprising 2 screens. (See [the JSON file for my tl;dr app][example-tldr-json] for an example).

Another example might be based on some device capability; if your your Theremin app requires devices with a TrueDepth
camera, you might want one device set for compatible phones, containing actual screenshots, and another device set for
incompatible phones which simply shows a compatibility notice.

# Using pretty frames from Facebook

Facebook have created some beautiful pixel-perfect frames for a whole load of devices, and you can use these frames
for your screenshots. First, you'll need to [download the images from Facebook][facebook-devices], then specify their
path using the `-f` switch.

```
./generator.rb -f ~/Facebook\ Devices/ ...
```

If you want a frame containing a small bottom shadow, use the `-s` flag:

```
./generator.rb -f ~/Facebook\ Devices/ -s ...
```

The output looks like this:

![examples of Facebook device frames][example-facebook-device-png]


# Changing frame colours

You can change the colour of frames like this:

```
"frame_colours": {
  "iphone_se": "rose_gold",
  "iphone_8": "gold",
  "iphone_8plus": "gold",
  "iphone_xs": "silver",
  "iphone_xs_max": "space_grey"
}
```

When using Facebook device frames, you can use values like `rose_gold` and `space_grey`. (See [devices.rb] for the supported
colours). If you're not using Facebook devices, then the colours can be hex values. (example: `#000000`)


[frameit]: https://docs.fastlane.tools/actions/frameit/
[snapshot]: https://docs.fastlane.tools/actions/snapshot/
[deliver]: https://docs.fastlane.tools/actions/deliver/
[tldr-app]: https://apps.apple.com/us/app/tldr-pages/id1071725095
[example1-png]: docs/example1.png
[example-minimal-json]: examples/minimal.json
[example2-png]: docs/example2.png
[devices.rb]: devices.rb
[example-tldr-json]: examples/tldr-example.json
[example-screenshot-y-location-png]: docs/example-screenshot-y-location.png
[example-screenshot-x-location-png]: docs/example-screenshot-x-location.png
[example-screenshot-scale-png]: docs/example-screenshot-scale.png
[example-screenshot-rotation-png]: docs/example-screenshot-rotation.png
[example-text-alignment-png]: docs/example-text-alignment.png 
[example-text-y-location-png]: docs/example-text-y-location.png
[example-text-styles-png]: docs/example-text-styles.png
[example-image-import]: docs/example-image-import.png
[facebook-devices]: https://facebook.design/devices
[example-facebook-device-png]: docs/example-facebook-device.png
