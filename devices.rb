# :width and :height are the native resolution of the device
# :gap is the gap (at design time) between screenshots
# :output_name_tag is sometimes used when saving the output files, which can help Fastlane's
# 'deliver' tool to differentiate between screenshots for different devices
# :device_frame is only used when we're framing screenshots in Facebook device artwork
Device = Struct.new(:width, :height, :gap, :output_name_tag, :device_frame)

# if you're using Facebook's device artwork, then:
# :sub_directory is the subdirectory which holds the images for this device
# :sub_directory_with_shadow is the subdirectory which holds the images for this device
# :device_x, :device_y is the top-left coordinate where we crop the image
# :device_width, :device_height is the size of image to crop
# :screen_x, :screen_y is the location of the screen (where we'll paste our screenshot)
# *after* cropping. The size of the screen, obviously, is the same as the native
# resolution of the device, so we don't need to specify that in the DeviceFrame struct
DeviceFrame = Struct.new(:sub_directory,
                         :sub_directory_with_shadow,
                         :device_x, :device_y, :device_width, :device_height,
                         :screen_x, :screen_y,
                         :colours)

class Devices
  def initialize
    @all_devices = {
        # TODO: gap 58 is a guess
        :iphone_se => Device.new(640, 1136, 58, '',
                                 DeviceFrame.new('/Phones/Apple iPhone SE/Device/',
                                                 '/Phones/Apple iPhone SE/Device with Shadow/',
                                                 36, 43, 760, 1594,
                                                 64, 237, {
                                                     :gold => 'Apple iPhone SE Gold.png',
                                                     :rose_gold => 'Apple iPhone SE Rose Gold.png',
                                                     :silver => 'Apple iPhone SE Silver.png',
                                                     :space_grey => 'Apple iPhone SE Space Gray.png',
                                                     :space_gray => 'Apple iPhone SE Space Gray.png'
                                                 })),
        :iphone_8 => Device.new(750, 1334, 58, '',
                                DeviceFrame.new('/Phones/Apple iPhone 8/Device/',
                                                '/Phones/Apple iPhone 8/Device with Shadow/',
                                                39, 61, 871, 1776,
                                                61, 219, {
                                                    :gold => 'Apple iPhone 8 Gold.png',
                                                    :silver => 'Apple iPhone 8 Silver.png',
                                                    :space_grey => 'Apple iPhone 8 Space Grey.png',
                                                    :space_gray => 'Apple iPhone 8 Space Grey.png'
                                                })),
        :iphone_8plus => Device.new(1242, 2208, 52, '',
                                    DeviceFrame.new('/Phones/Apple iPhone 8 Plus/Device/',
                                                    '/Phones/Apple iPhone 8 Plus/Device with Shadow/',
                                                    102, 65, 1436, 2876,
                                                    98, 335, {
                                                        :gold => 'Apple iPhone 8 Plus Gold.png',
                                                        :silver => 'Apple iPhone 8 Plus Silver.png',
                                                        :space_grey => 'Apple iPhone 8 Plus Space Grey.png',
                                                        :space_gray => 'Apple iPhone 8 Plus Space Grey.png'
                                                    })),
        :iphone_xs => Device.new(1125, 2436, 56, '',
                                 DeviceFrame.new('/Phones/Apple iPhone XS/Device/',
                                                 '/Phones/Apple iPhone XS/Device with Shadow/',
                                                 53, 43, 1295, 2590,
                                                 77, 77, {
                                                     :gold => 'Apple iPhone XS Gold.png',
                                                     :silver => 'Apple iPhone XS Silver.png',
                                                     :space_grey => 'Apple iPhone XS Space Grey.png',
                                                     :space_gray => 'Apple iPhone XS Space Grey.png'
                                                 })),
        # TODO: gap 56 is a guess
        :iphone_xs_max => Device.new(1242, 2688, 56, '',
                                     DeviceFrame.new('/Phones/Apple iPhone XS Max/Device/',
                                                     '/Phones/Apple iPhone XS Max/Device with Shadow/',
                                                     54, 60, 1413, 2844,
                                                     86, 78, {
                                                         :gold => 'Apple iPhone XS Max Gold.png',
                                                         :silver => 'Apple iPhone XS Max Silver.png',
                                                         :space_grey => 'Apple iPhone XS Max Space Grey.png',
                                                         :space_gray => 'Apple iPhone XS Max Space Grey.png'
                                                     })),
        # TODO: gap 56 is a guess
        :ipad_pro_12_9_2nd_gen => Device.new(2048, 2732, 56, '',
                                             DeviceFrame.new('/Tablets/Apple iPad Pro/Device/',
                                                             '/Tablets/Apple iPad Pro/Device with Shadow/',
                                                             81, 134, 2286, 3168,
                                                             119, 216, {
                                                                 :gold => 'Apple iPad Pro Gold.png',
                                                                 :silver => 'Apple iPad Pro Silver.png',
                                                                 :space_grey => 'Apple iPad Pro Space Gray.png',
                                                                 :space_gray => 'Apple iPad Pro Space Gray.png'
                                                             })),
        # TODO: gap 56 is a guess
        :ipad_pro_12_9_3rd_gen => Device.new(2048, 2732, 56, '_ipadPro129', nil)
    }
  end

  def all_devices
    @all_devices
  end
end
