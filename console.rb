class Console
  def initialize(verbosity)
    @@verbosity = verbosity
  end

  def self.log_warn(message)
    puts("W: #{message}")
  end

  def self.log_info(message)
    puts("I: #{message}")
  end

  def self.log_debug(message)
    puts("D: #{message}") if @@verbosity.count > 0
  end

  def self.log_fine(message)
    puts("F: #{message}") if @@verbosity.count > 1
  end
end

def log_warn(message)
  Console.log_warn(message)
end

def log_info(message)
  Console.log_info(message)
end

def log_debug(message)
  Console.log_debug(message)
end

def log_fine(message)
  Console.log_fine(message)
end

def app_fail(message)
  puts("E: #{message}")
  exit 1
end
