require_relative 'console'
require_relative 'sources'

require "mini_magick"
require 'fileutils'

class Canvas
  def initialize(canvas_scale, device_name, device, screen_count, background)
    @device_name = device_name
    @device = device
    @device_width = device[:width]
    @device_height = device[:height]
    @screen_count = screen_count

    # calculate the bounds of the individual screenshots (device_width and
    # device_height) and the total canvas bounds (including gaps between
    # screenshots)
    @scaled_device_width = @device_width * canvas_scale
    @scaled_device_height = @device_height * canvas_scale
    @gap = @device[:gap] * canvas_scale
    @canvas_width = (@screen_count * @scaled_device_width) + ((@screen_count - 1) * @gap)
    canvas_height = @scaled_device_height

    gradient_angle = background.fetch(:gradient_angle, 0)

    image_data = MiniMagick::Tool::Convert.new do |i|
      i.size "#{@canvas_width}x#{canvas_height}"
      i << '-define' << "gradient:angle=#{gradient_angle}"
      i.gradient "#{background[:gradient_from]}-#{background[:gradient_to]}"
      i << 'png:-'
    end
    @image = MiniMagick::Image.read(image_data)
  end

  def add_frame(frame, source_images, all_devices, pretty_frames, frame_colour)
    log_debug("Adding frame for image #{frame[:source]}")

    # a frame with scale 1 is 80% the size of the screenshot image
    # this is the total size of the image we're adding, including its outer frame
    scale = frame.fetch(:scale, 1) * 0.8
    frame_width = @scaled_device_width * scale
    frame_height = @scaled_device_height * scale

    # make the image that will be added to our main @image
    if pretty_frames.nil?
      frame_image = plain_frame(frame, frame_width, frame_height, source_images, frame_colour)
    else
      frame_image = pretty_frame(frame, frame_width, frame_height, source_images, all_devices, pretty_frames, frame_colour)
    end

    # now we can add this frame_image to the main @image
    y_location = frame.fetch(:y_location, 0)
    top_gap = @scaled_device_height * 0.25
    adjustable = @scaled_device_height - top_gap

    translate_x = ((@scaled_device_width + @gap) * (frame[:x_location] - 1)) + (@scaled_device_width * 0.5) - (frame_image.width * 0.5)
    translate_y = top_gap + (adjustable * y_location)

    @image = @image.composite(frame_image) do |i|
      i.compose 'over'
      i.geometry "+#{translate_x}+#{translate_y}"
    end
  end

  def plain_frame_colour(colour_value)

    case colour_value
    when nil, 'black'
      colour = '#000000'
    when 'silver'
      colour = '#e6e6e6'
    when 'gold'
      colour = '#fce4c5'
    when 'rose gold'
      colour = '#f1d9d4'
    else
      if colour_value.start_with('#')
        colour = colour_value
      else
        colour = nil
        app_fail("unknown frame colour '#{colour_value}'")
      end
    end

    colour
  end

  # this returns a MiniMagick image, no larger than max_frame_width x max_frame_height
  def plain_frame(frame, max_frame_width, max_frame_height, source_images, frame_colour)
    # this is the thickness of the border around the screenshot, and the border radius
    # of the artificial frame
    border = max_frame_width / 20
    corner_radius = max_frame_width / 20

    # we know the _maximum_ width and height of the whole image we're inserting, including its
    # frame, so calculate the maximum width and height of the screenshot _inside_ the frame
    max_screenshot_width = max_frame_width - (border * 2)
    max_screenshot_height = max_frame_height - (border * 2)

    # load the input image - the actual screenshot image
    input_image = source_images.image(frame[:source], @device_width, @device_height)

    # now, if we were to use this screenshot (input_image) at its natural size, how many times too
    # big would it be, for width and height?
    # then use the largest of these two ratios to work out how much we need to shrink the image by,
    # to retain its aspect ratio, but be no larger than max_screenshot_width x max_screenshot_height
    input_image_geometry = input_image.data['geometry']
    natural_width_ratio = (input_image_geometry['width'] / max_screenshot_width)
    natural_height_ratio = (input_image_geometry['height'] / max_screenshot_height)
    largest_ratio = [natural_width_ratio, natural_height_ratio].max

    # now we can resize the screenshot image to its optimum size
    screenshot_width = input_image_geometry['width'] / largest_ratio
    screenshot_height = input_image_geometry['height'] / largest_ratio
    input_image.resize("#{screenshot_width}x#{screenshot_height}")
    log_fine("screenshot image size: #{screenshot_width},#{screenshot_height}")

    # now for the fake frame

    frame_width = screenshot_width + (border * 2)
    frame_height = screenshot_height + (border * 2)

    # make a new image of the correct size for this frame
    frame_image_data = MiniMagick::Tool::Convert.new do |i|
      i.size "#{frame_width}x#{frame_height}"
      i.xc 'none'
      i.fill plain_frame_colour(frame_colour)
      i.draw "roundRectangle 0,0 #{frame_width},#{frame_height} #{corner_radius},#{corner_radius}"
      i << 'png:-'
    end
    frame_image = MiniMagick::Image.read(frame_image_data)
    log_fine("frame image size: #{frame_image.data['geometry']['width']},#{frame_image.data['geometry']['height']}")

    # now put the input image (screenshot) into the frame
    frame_image = frame_image.composite(input_image) do |i|
      i.compose 'over'
      i.geometry "+#{border}+#{border}"
    end

    # rotate the whole image
    frame_image.combine_options do |b|
      b.background "none"
      b.rotate frame.fetch(:rotation, 0)
    end

    frame_image
  end

  # this returns a MiniMagick image, containing a real screenshot inside a frame
  # no larger than max_frame_width x max_frame_height
  # the screenshot used will ideally be the same dimensions as our output image,
  def pretty_frame(frame, max_frame_width, max_frame_height, source_images, all_devices, pretty_frames, frame_colour)
    # this is the partial filename for our source screenshot image
    source_partial_name = frame[:source]
    log_debug("Making pretty frame for output #{@device_name}, input #{source_partial_name}")

    # now find the best size of pretty frame, for the output device, based on the
    # available screenshots
    best_frame = pretty_frames.best_frame(source_images, source_partial_name, all_devices, @device_width, @device_height)
    best_frame_device = all_devices[best_frame]
    best_frame_display_width = best_frame_device[:width]
    best_frame_display_height = best_frame_device[:height]

    log_info("Selected #{best_frame} for partial_name #{source_partial_name}")
    log_warn("Screenshot '#{source_partial_name}' doesn't exist in #{@device_name} size, using #{best_frame} size instead") if best_frame.to_s != @device_name

    # start by getting the pretty frame image for the preferred device and colour
    frame_image = pretty_frames.frame(best_frame, best_frame_device, frame_colour)

    # now, if we were to use this pretty frame at its natural size, how many times too
    # big would it be, for width and height?
    # then use the largest of these two ratios to work out how much we need to shrink the image by,
    # to retain its aspect ratio, but be no larger than max_frame_width x max_frame_height
    frame_image_geometry = frame_image.data['geometry']
    original_frame_width = frame_image_geometry['width']
    original_frame_height = frame_image_geometry['height']

    # now make a blank image which is the correct size for the whole image
    result_image_data = MiniMagick::Tool::Convert.new do |i|
      i.size "#{original_frame_width}x#{original_frame_height}"
      i.xc 'none'
      i << 'png:-'
    end
    result_image = MiniMagick::Image.read(result_image_data)

    # this is the location of the top-left corner of the screen inside the pretty frame,
    # if the pretty frame is at its original size
    frame_metrics = best_frame_device[:device_frame]
    unscaled_device_screen_x = frame_metrics[:screen_x] + frame_metrics[:device_x]
    unscaled_device_screen_y = frame_metrics[:screen_y] + frame_metrics[:device_y]

    # now load the screenshot image, which will be inserted into the frame
    input_image = source_images.image(source_partial_name, best_frame_display_width, best_frame_display_height)

    # ... put the image (screenshot) into the frame, at unscaled_device_screen_x,unscaled_device_screen_y
    result_image = result_image.composite(input_image) do |i|
      i.compose 'over'
      i.geometry "+#{unscaled_device_screen_x}+#{unscaled_device_screen_y}"
    end

    # ... and put the pretty frame on top, at 0,0
    result_image = result_image.composite(frame_image) do |i|
      i.compose 'over'
      i.geometry "+0+0"
    end

    # now we can scale the result image to its optimum size
    natural_width_ratio = (frame_metrics[:device_width] / max_frame_width)
    natural_height_ratio = (frame_metrics[:device_height] / max_frame_height)
    largest_ratio = [natural_width_ratio, natural_height_ratio].max

    result_image_width = original_frame_width / largest_ratio
    result_image_height = original_frame_height / largest_ratio
    result_image.resize("#{result_image_width}x#{result_image_height}")
    log_fine("pretty frame image size: #{result_image_width},#{result_image_height}")

    # finally, rotate the whole image
    result_image.combine_options do |b|
      b.background "none"
      b.rotate frame.fetch(:rotation, 0)
    end

    result_image
  end

  def add_image(image_object)
    # load the image, and calculate how to scale it
    image = MiniMagick::Image.open(image_object[:source])
    image_geometry = image.data['geometry']
    natural_width_ratio = (image_geometry['width'] / @scaled_device_width.to_f)
    natural_height_ratio = (image_geometry['height'] / @scaled_device_height.to_f)
    largest_ratio = [natural_width_ratio, natural_height_ratio].max
    scale = image_object.fetch(:scale, 1)

    # now scale the image
    scaled_image_width = image_geometry['width'] * scale / largest_ratio
    scaled_image_height = image_geometry['height'] * scale / largest_ratio
    image.resize("#{scaled_image_width}x#{scaled_image_height}")

    # where to put the image?
    horizontal_excess = @scaled_device_width - scaled_image_width
    vertical_excess = @scaled_device_height - scaled_image_height
    x = ((image_object[:screen] - 1) * (@scaled_device_width + @gap)) + (horizontal_excess * image_object.fetch(:x_location, 0))
    y = vertical_excess * image_object.fetch(:y_location, 0)

    # compose returns a new image - it doesn't mutate the current one
    @image = @image.composite(image) do |c|
      c.compose 'over'
      c.geometry "+#{x}+#{y}"
    end
  end

  def add_text(text_object, language, style)
    text = text_object[language.to_sym]
    return if text.nil? || text.empty?

    log_debug("Adding text '#{text.tr("\n"," ")}'")

    alignment = text_object.fetch(:alignment, 'centre')
    y_location = text_object.fetch(:y_location, 0)

    # must escape text before it's passed to ImageMagick
    escaped_text = text.tr("\\", "\\\\").tr("'", "")

    gravity = ''

    # first, draw the text into a separate image, and crop it
    case alignment
    when 'left'
      gravity = 'west'
    when 'right'
      gravity = 'east'
    when 'centre', 'center'
      gravity = 'center'
    else
      app_fail("Unrecognised alignment '#{text_object[:alignment]}'")
    end

    font_size = style[:size] * @scaled_device_width * 0.1

    text_image_data = MiniMagick::Tool::Convert.new do |i|
      i.size "#{@scaled_device_width * 1.1}x#{@scaled_device_height * 1.1}"
      i.xc 'none'
      i.font "#{style[:font]}" unless style[:font].nil?
      i.pointsize font_size
      i.gravity gravity
      i.fill style[:colour]
      i.annotate 0, escaped_text
      i.trim
      i << 'png:-'
    end
    text_image = MiniMagick::Image.read(text_image_data)

    # now we know the size of this text image, we can insert
    # this text image into the result image
    resolution = text_image.data['geometry']
    text_width = resolution['width']
    text_height = resolution['height']
    margin_horizontal = [@scaled_device_height, @scaled_device_width].max * 0.03
    margin_vertical = margin_horizontal * 2

    log_warn("Text too wide: '#{text.split("\n").first}'") if text_width > (@scaled_device_width + margin_horizontal + margin_horizontal)
    log_warn("Text too tall: '#{text.split("\n").first}'") if text_height > (@scaled_device_height+ margin_vertical + margin_vertical)

    adjustable = @scaled_device_height - (margin_vertical * 2) - text_height
    y = margin_vertical + (adjustable * y_location)
    x = 0

    case alignment
    when 'left'
      x = (text_object[:screen] - 1) * (@scaled_device_width + @gap) + margin_horizontal
    when 'right'
      x = ((text_object[:screen] - 1) * (@scaled_device_width + @gap)) + @scaled_device_width - text_width - margin_horizontal
    when 'centre', 'center'
      x = ((text_object[:screen] - 1) * (@scaled_device_width + @gap)) + (@scaled_device_width * 0.5) - (text_width / 2)
    end

    # compose returns a new image - it doesn't mutate the current one
    @image = @image.composite(text_image) do |c|
      c.compose 'over'
      c.geometry "+#{x}+#{y}"
    end
  end

  def save(language, whole_canvas, output_image_directory)
    FileUtils.mkdir_p ("#{output_image_directory}/#{language}") unless Dir.exist?("#{output_image_directory}/#{language}")

    output_files = []

    if whole_canvas
      for screen in 0...@screen_count do
        x1 = screen * (@scaled_device_width + @gap)
        y1 = 0
        x2 = x1 + @scaled_device_width - 1
        y2 = y1 + @scaled_device_height - 1

        @image.combine_options do |c|
          c.fill 'none'
          c.draw "stroke white rectangle #{x1},#{y1} #{x2},#{y2}"
        end
      end

      output_filename = "#{language}/#{@device_name}#{@device[:output_name_tag]}.png"
      output_filepath = "#{output_image_directory}/#{output_filename}"
      @image.write(output_filepath)
      log_info("Wrote to #{output_filepath}")

      output_files << output_filename
    else
      for screen in 0...@screen_count do
        x = screen * (@scaled_device_width + @gap)
        screen_image = MiniMagick::Image.open(@image.path)
        screen_image.crop("#{@scaled_device_width}x#{@scaled_device_height} +#{x}+0")

        output_filename = "#{language}/#{@device_name}#{@device[:output_name_tag]}_#{screen}_framed.png"
        output_filepath = "#{output_image_directory}/#{output_filename}"
        screen_image.write(output_filepath)
        log_info("Wrote to #{output_filepath}")

        output_files << output_filename
      end
    end

    return {:device => @device_name,
            :language => language,
            :gap => @gap,
            :canvas_width => @canvas_width,
            :image_width => @scaled_device_width,
            :image_height => @scaled_device_height,
            :output_files => output_files}
  end
end